package ru.t1.nikitushkina.tm.command.project;

import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECTS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

}
