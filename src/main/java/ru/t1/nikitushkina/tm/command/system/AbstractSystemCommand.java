package ru.t1.nikitushkina.tm.command.system;

import ru.t1.nikitushkina.tm.api.service.ICommandService;
import ru.t1.nikitushkina.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
