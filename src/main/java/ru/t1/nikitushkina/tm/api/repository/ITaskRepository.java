package ru.t1.nikitushkina.tm.api.repository;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name, String description);

    Task create(String name);

    List<Task> findAllByProjectId(String projectId);

}
